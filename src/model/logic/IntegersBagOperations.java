package model.logic;

import java.util.Iterator;

import model.data_structures.IntegersBag;

public class IntegersBagOperations {



	public double computeMean(IntegersBag bag){
		double mean = 0;
		int length = 0;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				mean += iter.next();
				length++;
			}
			if( length > 0) mean = mean / length;
		}
		return mean;
	}


	public int getMax(IntegersBag bag){
		int max = Integer.MIN_VALUE;
		int value;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				value = iter.next();
				if( max < value){
					max = value;
				}
			}

		}
		return max;
	}

	/**
	 * Método que retorna el valor mínimo de la bolsa de enteros entregada por parámetro
	 * @param bag Bolsa de enteros de la que se quiere conocer el valor mínimo.
	 * @return min Valor mínimo de la bolsa de enteros. Si la bolsa está vacía, retorna el valor máximo de la clase Integer.
	 */
	public int getMin(IntegersBag bag){
		int min = Integer.MAX_VALUE;
		int value;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				value = iter.next();
				if(min > value){
					min = value;
				}
			}
		}

		return min;
	}

	/**
	 * Método que retorna el valor de la suma de todos los enteros encontrados en la bolsa.
	 * @param bag Bolsa de enteros de la que se quiere conocer su suma.
	 * @return sum Valor de la suma de todos los enteros encontrados en la bolsa.
	 */
	public int getSum(IntegersBag bag){
		int sum = 0;
		int actual;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				actual = iter.next();
				sum += actual;
			}
		}

		return sum;
	}

	/**
	 * Método que retorna el valor de la multiplicación de todos los enteros (excluyendo al cero) encontrados en la bolsa.
	 * @param bag Bolsa de enteros de la que se quiere conocer su multiplicación.
	 * @return sum Valor de la multiplicación de todos los enteros encontrados en la bolsa (excluyendo al cero).
	 */
	public int getMult(IntegersBag bag){
		int mult = 1;
		int actual;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				actual = iter.next();
				if(actual != 0){
					mult *= actual;
				}
			}
		}

		return mult;
	}
}
